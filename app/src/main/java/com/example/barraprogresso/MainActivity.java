package com.example.barraprogresso;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {
    private ProgressBar progressBarHorizontal;
    private ProgressBar progressBarCircular;
    private int progresso = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBarHorizontal = findViewById(R.id.progressHorizontal);
        progressBarCircular = findViewById(R.id.progressCircular);
    }

    public void carregarProgressBar(View view){
            this.progresso = this.progresso + 1;
            progressBarHorizontal.setProgress(this.progresso);
            if(this.progresso == 10){
                progressBarCircular.setVisibility(View.INVISIBLE);
            }
    }

}